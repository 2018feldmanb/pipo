import wave, struct
import numpy as np

# Ouverture du fichier wav a decrypter
wav_original = wave.open("gamme 2.wav",'rb')
 
nchannels = wav_original.getnchannels()     # Returns number of audio channels (1 for mono, 2 for stereo)
framerate = wav_original.getframerate()     # Returns sampling frequency
nframes   = wav_original.getnframes()       # Returns number of audio frames
 
# Decouper le fichier en plusieurs parties (une note par partie)
frequences, freq_gauss, se = [], [], []
larg_frame = 50
for posi in range(0,nframes,1000*larg_frame):
 
    # Sequence contenant une note
    wav_original.setpos(posi)
    donnee = wav_original.readframes(larg_frame)
    data = struct.unpack('%sh' % (larg_frame * nchannels), donnee)
 
    # Transformee de Fourier
    w     = np.fft.fft(data)
    sig   = np.real(w * w.conjugate())
    freqs = np.fft.fftfreq(len(w)) * framerate
 
    # Estimation de la frequence
    idx = np.argmax(sig)
    f0, maxi = np.abs(freqs[idx]), sig[idx]
    frequences.append( f0 )
    se.append(sig)

wav_original.close()
print(nframes)
print(frequences)
print(se)
