import numpy as np
import wave, struct



#Ouverture du fichier wav à décrypter
wav_original = wave.open("/Users/selimboutlane/PycharmProjects/pipo/Accord.wav",'rb')
(nchannels, sampwidth, framerate, nframes, comptype, compname) = wav_original.getparams()



#Maintenant, on applique la transformée de Fourier
wav = wave.open("/Users/selimboutlane/PycharmProjects/pipo/Accord.wav",'rb')
(nchannels, sampwidth, framerate, nframes, comptype, compname) = wav.getparams()
frames = wav.readframes(nframes)

data = struct.unpack('%sh' % (nframes * nchannels), frames)

w = np.fft.fft(data)
freqs = np.fft.fftfreq(len(w))
idx=np.argmax(np.abs(w)**2)
freq=freqs[idx]
frequence=abs(freq*framerate)

print(frequence)
