import wave, struct
import numpy as np

# Ouverture du fichier wav a decrypter
wav_original = wave.open("gamme 2.wav",'rb')

nchannels = wav_original.getnchannels()     # Returns number of audio channels (1 for mono, 2 for stereo)
framerate = wav_original.getframerate()     # Returns sampling frequency
nframes   = wav_original.getnframes()       # Returns number of audio frames

# Decouper le fichier en plusieurs parties (une note par partie)
def sige(pos):
    wav_original.setpos(pos)
    donnee = wav_original.readframes(1)
    data = struct.unpack('%sh' % (1 * nchannels), donnee)
    w     = np.fft.fft(data)
    return(np.real(w * w.conjugate()))



frequences= []
debut=0
while debut <= nframes:

    # Sequence contenant une note
    if sige(debut)[0]>10**4:
        fin=debut+1
        while sige(fin)[0]> 10**4:
            fin=fin+1
        wav_original.setpos(debut)
        donnee = wav_original.readframes(fin-debut)
        data = struct.unpack('%sh' % ((fin-debut) * nchannels), donnee)
        w     = np.fft.fft(data)
        sig   = np.real(w * w.conjugate())
        freqs = np.fft.fftfreq(len(w)) * framerate

    # Estimation de la frequence
        idx = np.argmax(sig)
        f0, maxi = np.abs(freqs[idx]), sig[idx]
        frequences.append( f0 )
        debut=fin
    debut=debut+1

wav_original.close()
print(nframes)
print(frequences)

