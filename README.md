## pipo
Django 2 et Python 3.7
## MVP:
- permettra d'acquérir les notes de musique d'un morceau monophonique 
- permettra d'écrire les notes sur une partition
- interface Web avec Django

## Utilisation :

- Via l'interface web, rentrer le fichier Mp3 dont on veut obtenir la partition
- Convertir ce fichier
- Un pdf de la partition se télécharge automatiquement sur votre device.

