#-*-coding:utf-8
from django.http import HttpResponse
from django.shortcuts import render
from .forms import DocumentForm
import os
from .models import Document
from .analyse_son import analyse
from .conversion_latex import partition_latex
from .freq_converter import tab_notes as converter

def upload_file(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            doc = Document(document = request.FILES['document'])
            doc.save()
            pdf_path = "media/"+doc.document.name+".pdf"
            tex_path = "media/"+doc.document.name+".tex"
            lfreq =  analyse("media/"+doc.document.name) #crée la liste des fréquences
            note = converter(lfreq) #convertit en liste de notes
            partition_latex("Pipo",tex_path,note) #écrit le fichier .tex
            os.system("pdflatex -output-directory media/tmp "+ tex_path) #compile le .tex
            f = open(pdf_path, "rb")
            response = HttpResponse(f.read(), content_type='application/pdf') #crée une requête HTTP appropriée
            response['Content-Disposition'] = 'attachment; filename='+pdf_path
            doc.document.delete() #cleanup de la database
            doc.delete()
            os.system("rm -r media/tmp") #cleanup server
            return response
    else:
        form = DocumentForm()

    return render(request, 'index.html', {'form':form})

