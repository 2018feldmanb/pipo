import numpy as np
import wave, struct
import librosa

filename = "E:/CentraleSupélec/SIP/CW/mystere4.wav"

Do=[32.70,65.41,130.8,261.6,523.3]
Ré=[36.71,73.42,146.8,293.7,587.3]
Mi=[41.20,82.41,164.8,329.6,659.3]
Fa=[43.65,87.31,174.6,349.2,698.5]
Sol=[49,98,196,392,784]
La=[55,110,220,440,880]
Si=[61.74,123.5,246.9,493.9,987.8]

#Fonction qui transforme une fréquence en note
def fr(x):
   d=np.abs(x-Do[0])
   res="Do"
   for i in range (5) :
       r=np.abs(x-Do[i])
       if r<d :
           res="Do"
           d=r
   for i in range (5) :
       r=np.abs(x-Ré[i])
       if r<d :
           res="Ré"
           d=r
   for i in range (5) :
       r=np.abs(x-Mi[i])
       if r<d :
           res="Mi"
           d=r
   for i in range (5) :
       r=np.abs(x-Fa[i])
       if r<d :
           res="Fa"
           d=r
   for i in range (5) :
       r=np.abs(x-Sol[i])
       if r<d :
           res="Sol"
           d=r
   for i in range (5) :
       r=np.abs(x-La[i])
       if r<d :
           res="La"
           d=r
   for i in range (5) :
       r=np.abs(x-Si[i])
       if r<d :
           res="Si"
           d=r
   return(res)

def tab_notes(L):
    res=[]
    for i in range (len(L)):
        res.append(fr(L[i]))
    return res



#Ouverture du fichier son à décrypter
def analyse(filename):
    data, sr = librosa.load(filename)

# Détection tempo
    onset_env = librosa.onset.onset_strength(data, sr=sr)
    tempo = librosa.beat.tempo(onset_envelope=onset_env, sr=sr)

#Longueur du fichier analysé
    longueur=int(60*sr/(tempo*8))

# On enlève la phase de silence du début du fichier
    while abs(data[0])==0 :
        data = data[1:]

    l_freq = []
    l_ampli = [0]
    t_notes = [0]
# On analyse séparément tous les blocs

    for i in range(0,len(data),longueur):
        #On applique la transformée de Fourier sur chaque bloc
        w = np.fft.fft(data[i:i+longueur])
        S = np.abs(w)**2 #Passage au module
        amplitude = sum(S)#Somme de tous les modules
        l_ampli.append(amplitude) #Liste des amplitudes
        #On repère la frame d'apparition de chaque nouvelle note
        #La nouvelle note est repérée par un maximun local d'amplitude
    for k in range(1,len(l_ampli)-1):
        if l_ampli[k-1]<l_ampli[k] and l_ampli[k]>l_ampli[k+1] :
            t_notes.append(k)
    #Multiplie chaque terme par la longueur d'un bloc pour avoir la numéro de la frame
    for k in range(len(t_notes)):
        t_notes[k]=longueur*t_notes[k]
    t_notes.append(len(data))
    #Analyse de la fréquence de chaque note séparément

    for k in range(1,len(t_notes)-1):
        #La fréqence de la note est assimilée à la fréquence de plus grande amplitude
        w = np.fft.fft(data[int((t_notes[k]+t_notes[k-1])/2):int((t_notes[k]+t_notes[k+1])/2)])

        freqs = np.fft.fftfreq(len(w))
        idx=np.argmax(np.abs(w)**2)
        freq=freqs[idx]
        frequence=abs(freq*sr)
        l_freq.append(frequence)

    return(l_freq)


