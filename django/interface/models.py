# -*- coding: utf-8 -*-
from django.db import models

class Document(models.Model):
    document = models.FileField(upload_to='tmp/',verbose_name='')
    uploaded_at = models.DateTimeField(auto_now_add=True)
