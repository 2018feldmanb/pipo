import os
# on convertit la liste de note fournie par freq_converter en un fichier .tex s'appuyant sur la bibliothèque de musixtex permettant d'afficher des partitions en latex

def partition_latex(author, title, note): #author et title str; note liste de str
    fichier=open(title,"w")
    fichier.write("\documentclass[12pt,a4paper]{article}"+"\n\\usepackage{musixtex}")
    fichier.write("\\author{"+author+"}")
    fichier.write("\n\\title{Votre transcription}")
    fichier.write("\n\\begin{document}")
    fichier.write("\n\maketitle")
    fichier.write("\n\\begin{music}"+"\n\startpiece")
    for n in note:
        fichier.write("\n\\Notes \qu{"+str(n)+"} \enotes \\bar")
    fichier.write("\n\Endpiece")
    fichier.write("\n\end{music}")
    fichier.write("\n\end{document}")
    fichier.close()

