from django.test import TestCase
from .analyse_son import analyse
from .freq_converter import tab_notes as converter

class AnalyseTestCase(TestCase):
    song = "media/tests/gamme_c3.mp3"
    lfreq = analyse(song)
    notes = converter(lfreq)
    self.assertEqual(notes,[-2,-1,0,1,2,3,4,5])
