import numpy as np

Do=[32.70]
Réb=[34.6]
Ré=[36.71]
Mib=[38.9]
Mi=[41.20]
Fa=[43.65]
Solb=[46.2]
Sol=[49]
Lab=[51.9]
La=[55]
Sib=[58]
Si=[61.74]

def update(N):
    res=[N[0]]
    for i in range (1,10):
        res.append(N[0]*(2**i))
    return(res)

notes = ['c','d','e','f','g','a','b']
dic = {}
for i in range(5):
	for note in notes:
		dic[note+str(i)] = -2+notes.index(note)+7*i

print(len(dic))


for key in dic:
    dic[key] -= 21



Do=update(Do)
Réb=update(Réb)
Ré=update(Ré)
Mib=update(Mib)
Mi=update(Mi)
Fa=update(Fa)
Solb=update(Solb)
Sol=update(Sol)
Lab=update(Lab)
La=update(La)
Sib=update(Sib)
Si=update(Si)



def fr(x):
   d=np.abs(x-Do[0])
   res="c"
   for i in range (5) :
       r=np.abs(x-Do[i])
       if r<d :
           res=dic["c"+str(i)]
           d=r
   for i in range (5) :
       r=np.abs(x-Ré[i])
       if r<d :
           res=dic["d"+str(i)]
           d=r
   for i in range (5) :
       r=np.abs(x-Mi[i])
       if r<d :
           res=dic["e"+str(i)]
           d=r
   for i in range (5) :
       r=np.abs(x-Fa[i])
       if r<d :
           res=dic["f"+str(i)]
           d=r
   for i in range (5) :
       r=np.abs(x-Sol[i])
       if r<d :
           res=dic["g"+str(i)]
           d=r
   for i in range (5) :
       r=np.abs(x-La[i])
       if r<d :
           res=dic["a"+str(i)]
           d=r
   for i in range (5) :
       r=np.abs(x-Si[i])
       if r<d :
           res=dic["b"+str(i)]
           d=r
   for i in range (5) :
       r=np.abs(x-Réb[i])
       if r<d :
           res="_b"
           d=r
   for i in range (5) :
       r=np.abs(x-Mib[i])
       if r<d :
           res="_e"
           d=r
   for i in range (5) :
       r=np.abs(x-Solb[i])
       if r<d :
           res="_g"
           d=r
   for i in range (5) :
       r=np.abs(x-Lab[i])
       if r<d :
           res="_a"
           d=r
   for i in range (5) :
       r=np.abs(x-Sib[i])
       if r<d :
           res="_b"
           d=r
   return(res)


def tab_notes(L):
    res=[]
    for i in range (len(L)):
        res.append(fr(L[i]))
    return res


